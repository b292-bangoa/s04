--Artists with 'd'
select * from artists where name like "%d%";

--Songs with <330 length
select * from songs where length < 330;

--Join albums and songs...
select albums.album_title, songs.song_name, songs.length from albums
	join songs on albums.id = songs.album_id;

--Join artists and albums...
select * from artists
	join albums on artists.id = albums.artist_id where album_title like "%a%";

--Sort albums...
select * from albums order by album_title desc limit 4;

--Join albums and songs...
select * from albums 
	join songs on albums.id = songs.album_id
	order by album_title desc;

--Join artists and albums...
select * from artists
	join albums on artists.id = albums.artist_id
	where artists.name like "%a%";