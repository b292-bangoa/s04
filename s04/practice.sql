CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50),
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR (50) NOT NULL,
	contact_number VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(100) NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("ezrealAD", "ez", "Jarro Lightfeather", "2147483647", "ez@mail", "Piltover");
INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("seraphine", "pass123", "Seraph", "2147483647", "sr@mail", "Ionia");
INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("lux", "lux123", "Luxania", "2147483647", "lx@mail", "Demacia");
INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("vik", "vic123", "Viktor", "2147483647", "vk@mail", "Zaun");
INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("jinx", "jinx123", "Jinx", "2147483647", "jn@mail", "Zaun");

CREATE TABLE reviews (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	review VARCHAR(500) NOT NULL,
	datetime_created DATETIME not null,
	rating INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

insert into reviews(user_id, review, datetime_created, rating) values (1, "The songs are okay. Worth the subscription", "2023-05-03 00:00:00", 5);
insert into reviews(user_id, review, datetime_created, rating) values (2, "The songs are meh. I want BLACKPINK", "2023-05-03 00:00:00", 1);
insert into reviews(user_id, review, datetime_created, rating) values (3, "Add Bruno Mars and Lady Gaga", "2023-03-23 00:00:00", 4);
insert into reviews(user_id, review, datetime_created, rating) values (4, "I want to listen to more k-pop", "2022-09-23 00:00:00", 3);
insert into reviews(user_id, review, datetime_created, rating) values (5, "Kindly add more OPM", "2023-02-01 00:00:00", 5);

select * from users
	join reviews on users.id = reviews.user_id
	where users.full_name like "%k%";

select * from users
	join reviews on users.id = reviews.user_id
	where users.full_name like "%x%";

select * from reviews
	join users on users.id = reviews.user_id;

select users.username, reviews.review from reviews
	join users on users.id = reviews.user_id;